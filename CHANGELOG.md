# CHANGELOG

## [rev_5j] 2023-04-11

> Shout out to **Eddie** from the **Super Metroid Speedrunning Discord** for requesting the addition of a SOCD filter circuit to this design!

### Added:
- **LIGHTS!** Footprints for optional LEDs and their related current limiting resistors have been added around the ABXY face buttons and D-pad.
- **INPUT FILTER!** Optional SOCD filter circuit to prevent simultaneous LEFT/RIGHT UP/DOWN D-pad inputs.
- Interactive HTML BoM has been added to the build outputs.

### Changed:
- KiCAD 7 is out and stable, so this project has been updated to the new format.
- Board silkscreen and other decorations have been prettied up and made as self-documenting as possible (I hope).
- Schematic revision number now matches PCB revision.
- Component data in schematic has been normalized as much as possible (I'm still learning, okay?), which should resolve any remaining BoM issues.

-----

## [rev_4a] 2022-10-19

> Credit for identifying the issues and requesting the changes in this release goes to Reddit user **u/Zronium**.  Thanks for the feedback!

### Fixed:
- **Issue:** BoM quantities were all `null`.
    - **Cause:** The collate function in KiCAD's BoM script looks for at least 2 matching symbol properties.
    - **Fix:** Added more properties (like manufacturer name, part number, rated voltage, tolerance, etc) to ICs and passives.  These will also help with choosing suitable replacements if the exact model speficied is not available.

### Added
- New custom footprint at J2 for the wire-to-board header salvaged from Nintendo OEM controllers.  

    > Nobody seems to know exactly what this part is (probably some custom Nintendo/Mitsumi part), but I went ahead and created a custom footprint for it using measurements from a part salvaged from one of my own broken controllers.  It's over in my [Custom KiCAD Footprints](https://bitbucket.org/taylormadeak/custom-kicad-footprints/) repo if you want to use it in your own designs.

-----

## [rev_3a] 2022-09-30
### Fixed:

**Issue:** Controller not recognized by games like DKC, Pieces, others.  Sometimes worked if holding the B button.  

> Shout-out to my nephew **Connor**, who discovered and reported this first issue when he tested *all* of my controllers with *each and every one of my SNES games*.  Such dedication!  

- **Cause:** DS on U2 was tied high with the other unused parallel inputs, causing a timing conflict with the B button input on the first SCK transition after SDL.
- **Fix:** Tie DS low on U2.  

**Issue:** PCB required light alteration (edge sanding) to fit inside the controller housing.  

- **Cause:** Edge_Cuts tolerances a little too tight.
- **Fix:** The board outline on Edge_Cuts has been modified to increase tolerances.

### Changed:
- Increased track width for VCC connections.  This is probably overkill for such a low current application, but this board has the space for it so why not?
- This public repo no longer carries a non-commercial license.  If you want to sell boards produced from this design, go for it.  Just make sure you tell people where you got the design. =)
- Fix wire color labels on Molex connector silkscreen.

-----

## [rev_2e] 2022-04-07
First public release.

-----