[![CC-BY-SA 4.0][cc-by-sa-shield]][cc-by-sa] 
[![Buy me a coffee!][buy-me-a-coffee-image]][buy-me-a-coffee]
[![Certified Open-Source Hardware][oshwa-cert-img]][oshwa-cert]

# README
## For the Impatient
Just need some Gerbers?  Fabrication zips for popular board houses can be found on the [Downloads](https://bitbucket.org/taylormadeak/snes-oem-controller-pcb-replacement/downloads/) page.

Other documentation, such as assembly instructions, can be found at the [main project website](https://taylormadeak.bitbucket.io).

[CHANGELOG](CHANGELOG.md)

## About This Project
This is a replacement PCB designed to fit **controllers** used with the Nintendo **Super Nintendo Entertainment System/Super Famicom**.  It's possible that this design may fit recent third-party clones of this device, but I have only tested it with the OEM controllers.

### Project Goals
1. Create as close to a "drop-in" replacement PCB as is reasonably feasible.
2. Use modern equivalents to the original components wherever possible.
3. Use common, *inexpensive*, easy-to-source components.
4. Build at least a modicum of ESD protection into the design so this doesn't happen again.
5. Make the design accessible to anyone without requiring expensive tools or a great deal of soldering experience.

### Design Features

The PCB design I offer in this project implements the following features:

- "Drop-in" replacement: This board has the same physical dimensions and shape as the original Nintendo/Mitsumi design, so it should fit inside every SNES/Super Famicom controller that Nintendo ever produced (and probably a fair number of third-party designs as well) with no modification required.
- Double-sided PCB with plated vias.
- Large ground plane fills on both sides to increase the likelihood that any ESD shocks will go directly to ground.
- Uses modern Texas Instruments CD4021B shift registers, which are readily available and have built-in ESD protection in the form of diode-clamped inputs.
    - "Why these specific shift registers," you ask?  Because that's what Nintendo actually used in their first-run SNES controllers, so we can be certain that these will be compatible with the SNES control deck internal circuitry.
- Individual pull-up resistors for all 16 data lines.  
    - In addition to providing the required pull-up voltage for the button inputs, pull-ups on SCK, SDL, and SDA makes this design compatible with European SNES control decks.
- Surface-mount components to ensure the assembled PCB fits inside the original controller housing without modification.
- Hand-solder friendly SMT pads and sizes.
- ***(NEW in rev_4a!)*** Second wire-to-board header footprint for re-using the original connector harvested from a dead SNES controller.
- ***(NEW in rev_5j!)*** Optional LEDs!
    - I'm particularly proud of this one: I've been dreaming of having lighted buttons on my SNES controllers since the mid-90's, so I'm stoked to have finally made it come true!
    - Translucent buttons [like those sold by Kitsch-Bent](https://store.kitsch-bent.com/products/custom-buttons-snes) are recommended for this mod. 
- ***(NEW in rev_5j!)*** Optional D-pad input filter logic circuit to prevent simultaneous opposing cardinal direction (SOCD) inputs.
    - This one is for you crazy speedrunners who want to build custom controllers.

### PCB Renders
![TMAK's Super Anti-Zappy Drop-In Controller PCB (Full-frontal)!](docs/img/pcb_render_top.png)

![TMAK's Super Anti-Zappy Drop-In Controller PCB (Rear-view)!](docs/img/pcb_render_bot.png)

## Story Time
I have a confession to make: I still play my Super NES.  A lot.  I also live in Alaska, where it's cold and very dry for much of the year.  Cold + Dry = Static Electricity.

After my wife and I fried two SNES controllers in a week, I realized I had a problem to solve.

Here's the thing: original SNES controllers aren't made any more, and the after-market ones you can get now?  Well...they're not great.  Sure, I could have fixed this OEM controller by ordering a Mitsumi V520B chip from Ali Express (as much of the community does), but those are *also* not made any more and the design flaws of the controller PCB just mean you're going to fry the dang thing again.

You know what I'm talking about, right?  This PCB right here:

![Actual scan of my poor fried 30-year-old SNES controller PCB](docs/img/snes_oem_pcb_scan.png)

This PCB is *cheap*.  Single sided, no ground plane fills, and no clamping circuit. This kind of design is just *begging* for an ESD event, so it's little wonder when exactly that occurs.

In all fairness to Nintendo and Mitsumi, this PCB design lasted almost **30 years** before finally dying of an ESD event.  Here's the thing that really chaps my ass: *90% of the original controller is still usable!*  The cable, shell, buttons, button membranes, they're all fine.  Why replace the whole controller for want of an obsolete shift register?

I prefer to keep this stuff out of the landfills and in gamer (read: my) hands, if at all possible.


I dug around in the Internet Tubes and was quite surprised to discover that, while there are schematics for the SNES controller to be found (which all use the 74LS165, for some reason), no one seems to have released a PCB design for public consumption.

So, here we are.

## Using This Repo
### Creating a PCB
If you just want to create a PCB from this design, you should be aware of the following:

- This project was created with [KiCad EDA 7](https://kicad.org).
- The PCB design rules have been modified to meet OSH Park specifications.

The steps for creating fabrication files from this project vary widely depending on your PCB fab house of choice, so I will not cover it here.  If you use **OSH Park**, please refer to their documentation on how to [generate Gerbers from KiCad](https://docs.oshpark.com/design-tools/kicad/generating-kicad-gerbers/).

### Making Changes
If you want to remix this design, make sure you clone this repo with the `--recurse-submodules` option to get the shared custom footprint libraries.

## Bill of Materials
### Board Components - Basic Function
These components are required for the most basic functionality of this controller.  As such, these are the only parts that are *absolutely required*.

|Ref|Mouser Part #|Mfg. Part #|Quantity|Description|Datasheet|
|---|-------------|-----------|--------|-----------|----------|
|U1, U2|[595-CD4021BM96](https://www.mouser.com/ProductDetail/595-CD4021BM96)|`CD4021BM96`|2|CMOS 8-Stage Static Shift Register|[Datasheet](http://www.ti.com/general/docs/suppproductinfo.tsp?distId=26&gotoUrl=http%3A%2F%2Fwww.ti.com%2Flit%2Fgpn%2Fcd4021b)|
|R1, R2|[774-745C101104JP](https://www.mouser.com/ProductDetail/774-745C101104JP)|`745C101104JP`|2|Resistor array, 100Kohms 50V 5%|[Datasheet](https://www.mouser.com/datasheet/2/96/74x-1133528.pdf)|
|C1, C2|[710-885012007076](https://www.mouser.com/ProductDetail/710-885012007076)|`885012007076`|2|Multilayer Ceramic Capacitors MLCC - SMD/SMT WCAP-CSGP 100V 10pF 0805 5%|[Datasheet](https://www.mouser.com/datasheet/2/445/885012007076-1727103.pdf)|

### Board Components - *Optional* LED Backlit Buttions Feature
These optional components are required for the LED-backlit buttons and D-pad feature:

|Ref|Mouser Part #|Mfg. Part #|Quantity|Description|Datasheet|
|---|-------------|-----------|--------|-----------|----------|
|D1, D2, D3, D4, D5, D6, D7, D8|[593-LSM0603443V](https://mou.sr/3Tpmgne)|`LSM0603443V`|8|Standard LED, SMD 0603 3V 330mcd WHITE|[Datasheet](https://www.mouser.com/datasheet/2/423/LSM0603443V-1379797.pdf)|
|R3, R4|[303-0805W8J0101T5E](https://mou.sr/3z6xbsV)|`0805W8J0101T5E`|2|Thick film resistor - SMD 0805 1/8W 5% 100R|[Datasheet](https://www.mouser.com/datasheet/2/1365/1-3044649.pdf)|

### Board Components - *Optional* SOCD Filter Feature
These optional components are required for the SOCD filter feature.

> **Note:** The decoupling capacitors in this table are *in addition to* the ones required for the shift registers.

|Ref|Mouser Part #|Mfg. Part #|Quantity|Description|Datasheet|
|---|-------------|-----------|--------|-----------|----------|
|U3, U4|[595-CD4011BM96](https://www.mouser.com/ProductDetail/595-CD4011BM96)|`CD4011BM96`|2|Quad 2-input NAND gates|[Datasheet](https://www.ti.com/lit/ds/symlink/cd4023b-mil.pdf)|
|C3, C4|[710-885012007076](https://www.mouser.com/ProductDetail/710-885012007076)|`885012007076`|2|Multilayer Ceramic Capacitors MLCC - SMD/SMT WCAP-CSGP 100V 10pF 0805 5%|[Datasheet](https://www.mouser.com/datasheet/2/445/885012007076-1727103.pdf)|


### Cable-to-Board Connector
Assuming that you are restoring an existing SNES controller, from which you were able to salvage the cable, you may note that the connecter used to interface the cable to the PCB is...uncommon.  I wasn't able to find a part number, manufacturer, or any other information regarding this specific connector, so I chose a commonly available Molex Mini-SPOX 5-pin connector as a replacement.  

> **New as of rev_4a:**
>
> It is now possible to use the OEM Nintendo/Mitsumi wire-to-board header salvaged from your broken controller PCB by using it to populate **J2** (and leaving J1 unpopulated).  This section becomes optional in that case.
>
> **Be advised:** It is not physically possible to populate both J1 and J2 at the same time.  This was an intentional design choice.


Here are the parts required to replace the connector on the cable so that it will interface with this board:


|Ref|Mouser Part #|Mfg. Part #|Quantity|Description|Datasheet|
|---|-------------|-----------|--------|-----------|----------|
|J1|[538-22-05-7055](https://www.mouser.com/ProductDetail/538-22-05-7055)|`22-05-7055`|1|Molex Mini-SPOX 5-pin horizontal header|[Datasheet](https://www.mouser.com/datasheet/2/276/3/0022057055_PCB_HEADERS-2830316.pdf)|
|N/A|[538-50-37-5053](https://www.mouser.com/ProductDetail/538-50-37-5053)|`50.37-5053`|1|Mini-SPOX 5-pin plug|[Datasheet](https://www.mouser.com/datasheet/2/276/0050375053_CRIMP_HOUSINGS-160255.pdf)|
|N/A|[538-39-00-0160-CT](https://www.mouser.com/ProductDetail/538-39-00-0160-CT)|`39-00-0160`|5|Mini-SPOX TERM 22-28G FEM|[Datasheet](https://www.mouser.com/datasheet/2/276/0039000160_CRIMP_TERMINALS-1372462.pdf)|


### Shoulder Buttons
Working on the same assumption that you are restoring that existing SNES controller, the existing shoulder button daughter boards will connect up with a bit of wire and function the exact same way.

If you need (or want) to fabricate new shoulder button boards, I offer a [SNES Shoulder Button Module](https://bitbucket.org/taylormadeak/snes-shoulder-button) design as well.  It's a separate KiCad project because of KiCad's one project == one PCB philosophy.

## Legal Disclaimers

1. This project is in no way affiliated with, licensed by, or endorsed by **Nintendo Co. Ltd.** or their respected partners.  The names **"Nintendo," "Super Nintendo Entertainment System," and "Super Famicom"** are all registered trademarks of Nintendo Co., Ltd. and used here for context reference only.

2. This project represents 100% original work, no patented designs were used or copied in its creation.

3. The creator of this Open Source Hardware project **ASSUMES NO LIABILITY** for material, physical, or immaterial damages caused by the use or non-use of this design.

## License
This original work is licensed under a
[Creative Commons Attribution-ShareAlike 4.0 International License][cc-by-sa].

[![CC-BY-SA 4.0][cc-by-sa-image]][cc-by-sa]

[![Open Source Hardware][oshwa-img]][oshwa-link]

[cc-by-sa]: http://creativecommons.org/licenses/by-sa/4.0/
[cc-by-sa-image]: https://licensebuttons.net/l/by-sa/4.0/88x31.png
[cc-by-sa-shield]: https://img.shields.io/badge/License-CC%20BY--SA%204.0-lightgrey.svg

[buy-me-a-coffee]: https://www.buymeacoffee.com/taylormadeak
[buy-me-a-coffee-image]: https://badgen.net/badge/icon/buymeacoffee?icon=buymeacoffee&label

[oshwa-img]: https://i0.wp.com/www.oshwa.org/wp-content/uploads/2014/03/oshw-logo-100-px.png?resize=95%2C100&ssl=1
[oshwa-link]: https://www.oshwa.org/

[oshwa-cert]: https://https://certification.oshwa.org/us002604.html
[oshwa-cert-img]: docs/img/certification-mark-US002604-wide.png